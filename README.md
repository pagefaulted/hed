[![Build Status](https://travis-ci.com/pagefaulted/hed.svg?branch=master)](https://travis-ci.com/pagefaulted/hed)
[![ktlint](https://img.shields.io/badge/code%20style-%E2%9D%A4-FF4081.svg)](https://ktlint.github.io/)

**HED** is an Http Exchange Data provider.

It allows the retrieval of US Stocks data from Financial APIs (e.g. YahooFinance).

## Configuration
The config lives in _application.conf_. The only required configuration at the moment is your YahooFinancials _API key_. To setup an account visit `https://rapidapi.com/apidojo/api/yahoo-finance1`.

Depending on what allowance your API gives you, you might want to tweak the Financial Data refresh rate via the `refreshIntervalMin` configuration in _application.conf_.

## Endpoints
All endpoints support the GET method.

### Quotes
Retrieve simple TICKER: BID_PRICE comma separated quotes for a set of tickers, in US Dollars.

```
/quotes/<comma,separated,tickers>
```

For example:
```
curl -v http://localhost:8080/quotes/gme,wday,race
mods@cariddi ~/development/hed
[23:00:18]$ > curl -v http://localhost:8080/quotes/wday,gme,race
*   Trying ::1:8080...
* Connected to localhost (::1) port 8080 (#0)
> GET /quotes/wday,gme,race HTTP/1.1
> Host: localhost:8080
> User-Agent: curl/7.77.0
> Accept: */*
>
* Mark bundle as not supporting multiuse
< HTTP/1.1 200 OK
< Date: Wed, 30 Jun 2021 22:00:28 GMT
< Server: ktor-server-core/1.6.0
< Content-Length: 23
< Content-Type: text/plain; charset=UTF-8
<
* Connection #0 to host localhost left intact
WDAY: 238.4,GME: 214.44,RACE: 206.05
```
