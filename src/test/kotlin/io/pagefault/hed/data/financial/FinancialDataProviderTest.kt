package io.pagefault.hed.data.financial

import io.pagefault.hed.routes.RandomQuote
import org.junit.jupiter.api.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class FinancialDataProviderTest {

    @Test
    fun `withMissingQuotesFiller should fill unknown quotes with UnknownQuote instances`() {
        val requestedTickers = setOf("wday", "gme", "race", "amd")

        // gme and amd are missing
        val filledResults = withMissingQuotesFiller(requestedTickers) {
            setOf(RandomQuote("wday"), RandomQuote("race"))
        }

        assertEquals(requestedTickers.size, filledResults.size)

        requestedTickers.forEach { requestedTicker ->
            assertTrue(filledResults.any { it.ticker == requestedTicker })
        }
    }
}
