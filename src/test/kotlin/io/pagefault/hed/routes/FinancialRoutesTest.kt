package io.pagefault.hed.routes

import io.ktor.http.HttpMethod.Companion.Get
import io.ktor.http.HttpStatusCode
import io.ktor.server.testing.handleRequest
import io.ktor.server.testing.withTestApplication
import io.pagefault.hed.data.financial.FinancialDataProvider
import io.pagefault.hed.data.financial.FinancialStore
import io.pagefault.hed.data.financial.Quote
import io.pagefault.hed.hedMain
import io.pagefault.hed.routes.FinancialRoutes.Routes.QUOTES
import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.koin.core.context.startKoin
import org.koin.core.context.stopKoin
import org.koin.dsl.module
import org.koin.test.KoinTest
import kotlin.random.Random
import kotlin.test.assertContains
import kotlin.test.assertEquals
import kotlin.test.assertNotNull

class FinancialRoutesTest : KoinTest {
    private val testModule = module {
        single<FinancialStore> { DummyFinancialStore() }
    }

    @BeforeEach
    fun b4() {
        startKoin {
            modules(testModule)
        }
    }

    @AfterEach
    fun after() = stopKoin()

    @Test
    fun `an unknown route should respond with BadRequest`() {
        withTestApplication {
            application.hedMain(true)
            val response = handleRequest(Get, "/unhandled/route")
            assertEquals(HttpStatusCode.BadRequest, response.response.status())

            val otherResponse = handleRequest(Get, "/unhandled")
            assertEquals(HttpStatusCode.BadRequest, otherResponse.response.status())
        }
    }

    @Test
    fun `a single quote request should be answered with the requested quote`() {
        withTestApplication {
            application.hedMain(true)
            val ticker = "wday"
            val response = handleRequest(Get, "/${QUOTES.path}/$ticker")
            assertEquals(HttpStatusCode.OK, response.response.status())
            val res = response.response.content
            assertNotNull(res)
            assertContains(res, ticker)
        }
    }

    @Test
    fun `a multi-quote request should be answered with the requested quotes`() {
        withTestApplication {
            application.hedMain(true)
            val tickers = "wday,amzn"
            val response = handleRequest(Get, "/${QUOTES.path}/$tickers")
            assertEquals(HttpStatusCode.OK, response.response.status())
            val res = response.response.content
            assertNotNull(res)
            tickers.split(",").forEach {
                assertContains(res, it)
            }
        }
    }
}

/**
 * Dummy FinancialStore. All quotes are rubbish.
 */
class DummyFinancialStore : FinancialStore {
    private val finProv = DummyFinancialDataProvider()

    override fun getQuotes(requestedTickers: Set<String>): Set<Quote> =
        runBlocking {
            finProv.getUSQuotes(requestedTickers)
        }
}

/**
 * Dummy implementation of [FinancialDataProvider] that returns random quotes for the provided
 * tickers.
 */
class DummyFinancialDataProvider : FinancialDataProvider {
    override suspend fun getUSQuotes(tickers: Set<String>): Set<Quote> =
        tickers.map {
            RandomQuote(it)
        }.toSet()
}

/**
 * A random quote, not far from reality.
 */
class RandomQuote(
    override val ticker: String,
    override val bidPrice: Double = Random.nextDouble(),
    override val floating: Long = Random.nextLong(),
    override val outstanding: Long = Random.nextLong(),
    override val shorted: Long = Random.nextLong(),
    override val shortPercentOfFloat: Double = Random.nextDouble(),
    override val cap: Long = Random.nextLong()
) : Quote
