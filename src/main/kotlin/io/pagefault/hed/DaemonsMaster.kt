package io.pagefault.hed

import kotlinx.coroutines.CoroutineName
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import mu.KotlinLogging

sealed class Daemon {
    abstract fun name(): String
    abstract val behavior: suspend () -> Unit
}

data class IODaemon(val name: String, val coroutine: suspend () -> Unit) : Daemon() {
    override fun name(): String = name
    override val behavior: suspend () -> Unit = coroutine
}

/**
 * It handles daemon coroutines lifecycle.
 */
class DaemonsMaster {
    private val jobs: MutableSet<Job> = mutableSetOf()

    @Synchronized
    fun createDaemon(daemon: Daemon) {
        LOG.info("Creating Daemon ${daemon.name()}")

        @Suppress("REDUNDANT_ELSE_IN_WHEN")
        val dispatch = when (daemon) {
            is IODaemon -> Dispatchers.IO
            else -> Dispatchers.Default
        }

        CoroutineScope(dispatch + CoroutineName(daemon.name()))
            .launch { daemon.behavior() }
            .also { jobs.add(it) }
    }

    @Synchronized
    fun slayAll() = jobs.forEach {
        it.cancel()
        jobs.remove(it)
    }.also {
        LOG.info("All Daemons have been slayed.")
    }

    companion object {
        private val LOG = KotlinLogging.logger {}

        internal val INSTANCE: DaemonsMaster by lazy(LazyThreadSafetyMode.SYNCHRONIZED) {
            DaemonsMaster()
                .also { LOG.info("Daemons Master evoked.") }
        }
    }
}
