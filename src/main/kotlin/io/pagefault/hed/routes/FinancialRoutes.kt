package io.pagefault.hed.routes

import io.ktor.application.Application
import io.ktor.application.call
import io.ktor.http.HttpStatusCode
import io.ktor.response.respondText
import io.ktor.routing.Route
import io.ktor.routing.get
import io.ktor.routing.routing
import io.pagefault.hed.data.financial.FinancialStore
import io.pagefault.hed.data.financial.Quote
import io.pagefault.hed.routes.FinancialRoutes.Routes.QUOTES
import io.pagefault.hed.routes.FinancialRoutes.quotes
import mu.KotlinLogging
import org.koin.ktor.ext.inject

fun Application.financialRoutes() {
    routing {
        quotes()
    }
}

object FinancialRoutes {
    private val LOG = KotlinLogging.logger {}

    enum class Routes(val path: String) {
        QUOTES("quotes")
    }

    fun Route.quotes() {
        val finStore by inject<FinancialStore>()

        get("/${QUOTES.path}/{tickers}") {
            call.parameters["tickers"]!!
                .split(",")
                .toSet()
                .let {
                    LOG.info("quotes for tickers $it")
                    finStore.getQuotes(it)
                        .joinToString(" ", transform = Quote::asText)
                        .let {
                            call.respondText(status = HttpStatusCode.OK) { it }
                        }
                }
        }
    }
}
