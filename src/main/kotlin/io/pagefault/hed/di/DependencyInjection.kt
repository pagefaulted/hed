package io.pagefault.hed.di

import io.ktor.application.Application
import io.ktor.application.ApplicationEnvironment
import io.ktor.application.install
import io.ktor.features.CallLogging
import io.ktor.features.DefaultHeaders
import io.pagefault.hed.data.financial.DaemonicFinancialStore
import io.pagefault.hed.data.financial.YahooFinancialDataProvider
import io.pagefault.hed.data.financial.getYahooConfig
import io.pagefault.hed.utils.Configuration
import org.koin.core.KoinApplication
import org.koin.dsl.module
import org.koin.ktor.ext.Koin
import org.slf4j.event.Level

/**
 * Install application dependencies.
 */
fun Application.installDependencies() {
    Configuration.init(environment)

    install(CallLogging) {
        level = Level.INFO
    }
    install(DefaultHeaders)
    install(Koin) {
        getKoinDependencies(environment)
    }
}

/**
 * Retrieve a list of Koin dependencies to be injected.
 */
fun KoinApplication.getKoinDependencies(environment: ApplicationEnvironment): KoinApplication {
    val finStore = module(createdAtStart = true) {
        single {
            DaemonicFinancialStore(
                YahooFinancialDataProvider(environment.getYahooConfig()),
                Configuration.INSTANCE.financialsRefreshDelayMin
            ).withDaemon()
        }
    }
    return modules(finStore)
}
