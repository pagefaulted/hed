package io.pagefault.hed.data.financial

import kotlin.math.roundToInt

/**
 * Interface for the implementors of financial data providers.
 */
interface FinancialDataProvider {
    /**
     * Retrieve quotes from the US region for the tickers provided in the list.
     *
     * @param tickers the requested quotes.
     */
    suspend fun getUSQuotes(tickers: Set<String>): Set<Quote>
}

/**
 * Common interface for all quote implementations
 */
interface Quote {
    val ticker: String
    val bidPrice: Double
    val floating: Long?
    val outstanding: Long?
    val shorted: Long?
    val shortPercentOfFloat: Double?
    val cap: Long?

    /**
     * A low floating percentage indicates that most of the stocks are closely held.
     * (https://www.investopedia.com/terms/c/closely-held-stock.asp)
     */
    fun floatingPercentage(): Long =
        if (floating != null && outstanding != null) {
            (floating!! / outstanding!!) * 100
        } else {
            0
        }

    fun asText(): String = "$ticker: ${bidPrice.roundToInt()}"
}

data class UnknownQuote(override val ticker: String) : Quote {
    override val bidPrice: Double = 0.0
    override val floating: Long = 0
    override val outstanding: Long = 0
    override val shorted: Long = 0
    override val shortPercentOfFloat: Double = 0.0
    override val cap: Long = 0

    override fun toString(): String = "$ticker: (Unknown)"
}

fun withMissingQuotesFiller(
    requestedTickers: Set<String>,
    retrieveQuotesFn: () -> Set<Quote>
): Set<Quote> {

    val retrievedQuotes = retrieveQuotesFn()

    return requestedTickers
        .filterNot { requestedTicker ->
            retrievedQuotes.any { it.ticker.equals(requestedTicker, ignoreCase = true) }
        }.map { UnknownQuote(it) }
        .union(retrievedQuotes).toSet()
}
