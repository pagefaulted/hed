package io.pagefault.hed.data.financial

import com.beust.klaxon.Json
import com.beust.klaxon.JsonArray
import com.beust.klaxon.JsonObject
import com.beust.klaxon.Klaxon
import com.beust.klaxon.KlaxonException
import com.beust.klaxon.Parser
import io.ktor.application.ApplicationEnvironment
import io.ktor.client.HttpClient
import io.ktor.client.request.request
import io.ktor.client.statement.HttpResponse
import io.ktor.client.statement.readText
import io.ktor.http.HttpMethod
import mu.KotlinLogging
import org.koin.core.error.MissingPropertyException
import java.io.StringReader

private val LOG = KotlinLogging.logger {}

/**
 * Yahoo API to retrieve quotes from the US region.
 */
private const val GET_US_QUOTES = "https://apidojo-yahoo-finance-v1.p.rapidapi.com/market/v2/get-quotes?region=US&symbols="
private const val API_KEY_HEADER = "x-rapidapi-key"
private const val API_HOST_HEADER = "x-rapidapi-host"

/**
 * Financial Provider implementation for the Yahoo Finance API, through RapidAPI (https://rapidapi.com/apidojo/api/yahoo-finance1)
 */
class YahooFinancialDataProvider(private val config: YahooConfig) : FinancialDataProvider {
    override suspend fun getUSQuotes(tickers: Set<String>): Set<Quote> {
        val response: HttpResponse =
            HttpClient().use {
                it.request(
                    "$GET_US_QUOTES${tickers.joinToString("%2C")}"
                ) {
                    headers.append(API_KEY_HEADER, config.key)
                    headers.append(API_HOST_HEADER, config.host)
                    method = HttpMethod.Get
                }
            }

        val root: JsonObject = Parser.default().parse(StringReader(response.readText())) as JsonObject
        @Suppress("UNCHECKED_CAST")
        val responses = (root["quoteResponse"] as JsonObject)["result"] as JsonArray<JsonObject>?

        return if (responses == null) {
            tickers.map { UnknownQuote(it) }.toSet()
        } else {
            withMissingQuotesFiller(tickers) {
                responses
                    .mapNotNull {
                        try {
                            Klaxon().parseFromJsonObject<YahooFinanceQuote>(it)
                        } catch (ex: KlaxonException) {
                            LOG.warn("Could not parse json for quote $it", ex)
                            null
                        }
                    }.toSet()
            }
        }
    }
}

data class YahooFinanceQuote(
    @Json(name = "symbol") override val ticker: String,
    @Json(name = "bid") override val bidPrice: Double,
    @Json(name = "floatShares", serializeNull = false) override val floating: Long?,
    @Json(name = "sharesOutstanding", serializeNull = false) override val outstanding: Long?,
    @Json(name = "sharesShort", serializeNull = false) override val shorted: Long?,
    @Json(name = "shortPercentFloat", serializeNull = false) override val shortPercentOfFloat: Double?,
    @Json(name = "marketCap", serializeNull = false) override val cap: Long?
) : Quote

data class YahooConfig(val key: String, val host: String) {
    init {
        if (key.isEmpty() || host.isEmpty()) {
            throw MissingPropertyException("Yahoo api key and host must not be empty: $this")
        }
    }
}

fun ApplicationEnvironment.getYahooConfig(): YahooConfig =
    YahooConfig(
        config.property("ktor.yahooFinancial.apiKey").getString(),
        config.property("ktor.yahooFinancial.apiHost").getString()
    )
