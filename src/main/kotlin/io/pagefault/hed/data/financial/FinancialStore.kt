package io.pagefault.hed.data.financial

import io.pagefault.hed.DaemonsMaster
import io.pagefault.hed.IODaemon
import kotlinx.coroutines.InternalCoroutinesApi
import kotlinx.coroutines.NonCancellable.isActive
import kotlinx.coroutines.delay
import mu.KotlinLogging

/**
 * Store for financial data.
 */
interface FinancialStore {
    fun getQuotes(requestedTickers: Set<String>): Set<Quote>
}

/**
 * FinancialStore implementation that can be started as a Daemon.
 */
class DaemonicFinancialStore(
    private val financialDataProvider: FinancialDataProvider,
    private val refreshRateMinutes: Int,
    private var tickers: Set<String> = emptySet()
) : FinancialStore {
    private var quotes: Set<Quote> = emptySet()

    @OptIn(InternalCoroutinesApi::class)
    internal suspend fun updateQuotes() {
        while (isActive) {
            if (tickers.isNotEmpty()) {
                financialDataProvider.getUSQuotes(tickers)
                    .let { this.quotes = it }

                LOG.info("Quotes retrieved ${this.quotes.size}, going to sleep for $refreshRateMinutes minutes...")
                delay(refreshRateMinutes * 60 * 1000L)
            } else {
                // Shorter delay, waiting for the initial tickers request.
                LOG.info("Empty store, waiting for tickers...")
                delay(5 * 1000L)
            }
        }
    }

    override fun getQuotes(requestedTickers: Set<String>): Set<Quote> {
        LOG.info("Getting quotes for tickers: $requestedTickers")
        val quotes = quotes.filter {
            requestedTickers.map(String::uppercase).contains(it.ticker.uppercase())
        }.toSet()

        if (quotes.size != requestedTickers.size) {
            // Missing ticker, add to the quotes retrieved by the daemon
            LOG.info(
                "Some of the requested tickers are not available in this store at the moment. They will be" +
                    " retrieved at the next refresh."
            )
            tickers += requestedTickers.subtract(tickers).also { LOG.info("Missing tickers $it") }
        }
        return quotes
    }

    fun withDaemon(): FinancialStore =
        this.also {
            DaemonsMaster.INSTANCE.createDaemon(
                IODaemon("FinancialsDaemon", it::updateQuotes)
            )
        }

    companion object {
        private val LOG = KotlinLogging.logger {}
    }
}
