package io.pagefault.hed

import io.ktor.application.Application
import io.ktor.application.call
import io.ktor.http.HttpStatusCode
import io.ktor.response.respondText
import io.ktor.routing.get
import io.ktor.routing.routing
import io.pagefault.hed.di.installDependencies
import io.pagefault.hed.routes.financialRoutes
import mu.KotlinLogging

private val LOG = KotlinLogging.logger {}

fun main(args: Array<String>) = io.ktor.server.netty.EngineMain.main(args)

fun Application.hedMain(testing: Boolean = false) {
    if (!testing) {
        LOG.info("Installing dependencies")
        installDependencies()
    }

    LOG.info("Setting up routes")
    financialRoutes()
    unknown()
}

fun Application.unknown() {
    routing {
        get("/{unknown...}") {
            val route = call.parameters["unknown"]
            LOG.info("Unknown route requested: $route")
            call.respondText(status = HttpStatusCode.BadRequest) { "Unknown route \"$route\"" }
        }
    }
}
