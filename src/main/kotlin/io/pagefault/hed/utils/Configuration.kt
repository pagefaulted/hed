package io.pagefault.hed.utils

import io.ktor.application.ApplicationEnvironment

class Configuration(
    val financialsRefreshDelayMin: Int
) {

    companion object {
        lateinit var INSTANCE: Configuration

        fun init(environment: ApplicationEnvironment) {
            INSTANCE = Configuration(
                environment.config.property("ktor.financial.refreshIntervalMin").getString().toInt()
            )
        }
    }
}
